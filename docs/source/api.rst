API Reference
=============

.. rubric:: Description
.. automodule:: arduino_temperature_sensors
.. currentmodule:: arduino_temperature_sensors


.. rubric:: Modules
.. autosummary::
    :toctree: api

    arduino_temperature_sensors
    Daemon
    Monitor
    ArduinoBoard