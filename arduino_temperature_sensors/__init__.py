# -*- coding: utf-8 -*-
# Author: Carlos Vigo
# Contact: carlosv@phys.ethz.ch

""" Readout and logging application for the
EPIC Arduino Temperature Sensors.
"""

# Local imports
from . import __project__, arduino_temperature_sensors

__all__ = [
    __project__.__author__,
    __project__.__copyright__,
    __project__.__short_version__,
    __project__.__version__,
    __project__.__project_name__,
    'arduino_temperature_sensors'
]
